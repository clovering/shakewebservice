-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 11 月 18 日 11:25
-- 服务器版本: 5.5.16
-- PHP 版本: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE `shake` /*!40100 DEFAULT CHARACTER SET utf8 */

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `shake`
--

DELIMITER $$
--
-- 存储过程
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `userBatchInsert`()
BEGIN
    DECLARE i INT DEFAULT 10000;
    WHILE(i < 20000) DO
        INSERT INTO `_user` (`udid`, `password`, `name`, `avatar`, `level`, `exp`, `signature`, `source`, `time`) VALUES ('test1', 'test1123', 'test1', '/test/test1.jpg', 2, 167, '今年是2012年哦！', 0, '2012-11-10 15:23:00');
        SET i = i+1;
    END WHILE;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- 表的结构 `_user`
--

CREATE TABLE IF NOT EXISTS `_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `udid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL DEFAULT '0',
  `signature` text NOT NULL,
  `source` tinyint(1) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `source` (`source`),
  KEY `level` (`level`),
  KEY `time` (`time`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


CREATE TABLE IF NOT EXISTS `_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `poster` varchar(255) NOT NULL DEFAULT '',
  `availableBegin` datetime NOT NULL,
  `availableEnd` datetime NOT NULL,
  `createAt` datetime NOT NULL,
  `updateAt` datetime NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `availableBegin` (`availableBegin`),
  KEY `availableEnd` (`availableEnd`),
  KEY `createAt` (`createAt`),
  KEY `updateAt` (`updateAt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;	