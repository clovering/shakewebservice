$(function() {
	$('#avatarThumb a').live('click', function() {
		var id = $(this).attr('id');
		var _this = $('#avatarThumb').parent();
		$('#avatarThumb').remove();
		_this.append('<input type="file" name="avatarFile" value="" id="imageFile" style="position:static;" class="{required:true, accept:\'jpg,png,gif,jpeg,bmp\'}" />');
	});
});

$(function() {
	$('#posterThumb a').live('click', function() {
		var id = $(this).attr('id');
		var _this = $('#posterThumb').parent();
		$('#posterThumb').remove();
		_this.append('<input type="file" name="posterFile" value="" id="imageFile" style="position:static;" class="{required:true, accept:\'jpg,png,gif,jpeg,bmp\'}" />');
	});
});

jQuery.extend(jQuery.validator.messages, {
    required: "必选字段",
	remote: "账号已经存在",
	email: "请输入正确格式的电子邮件",
	url: "请输入合法的网址",
	date: "请输入合法的日期",
	dateISO: "请输入合法的日期 (ISO).",
	number: "请输入合法的数字",
	digits: "只能输入整数",
	creditcard: "请输入合法的信用卡号",
	equalTo: "请再次输入相同的值",
	accept: "请添加合法的文件",
	maxlength: jQuery.validator.format("请输入一个长度最多是 {0} 的字符串"),
	minlength: jQuery.validator.format("请输入一个长度最少是 {0} 的字符串"),
	rangelength: jQuery.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
	range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
	max: jQuery.validator.format("请输入一个最大为 {0} 的值"),
	min: jQuery.validator.format("请输入一个最小为 {0} 的值")
});