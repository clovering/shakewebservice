<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../inc/header.jsp"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>shake backend</title>
</head>
<body>

	<div id="page_header_wrapper">
		<div id="page_header">
			<div class="user">
				<a href="login.html">登录</a>
			</div>
			<div class="logo">shake</div>
		</div>
	</div>
	<div id="page_content">
		<div id="right_column">
			<div class="message"></div>
			<h1>登录shake移动互联网客户端管理后台</h1>
			<div class="block" style="padding:10px 20px">
				<form:form method="POST" commandName="manager">
					<div class="row">
						<label>用户名：</label> <input name="name" size="30" />
					</div>
					<div class="row">
						<label>密码：</label> <input name="password" size="30"
							type="password" />
					</div>
					<div class="row submit">
						<input type="submit" value="登录" />
					</div>
				</form:form>
			</div>
		</div>
		<div id="page_menu">
			<dl>
				<dt>
					用户管理<b></b>
				</dt>
				<dd>
					<a href="user-regist.html">注册用户</a>
				</dd>
				<dd>
					<a href="reportsVersion.html">腾讯微博</a>
				</dd>
				<dd>
					<a href="reportsChannel.html">新浪微博</a>
				</dd>
			</dl>
			<dl>
				<dt>
					用户分析<b></b>
				</dt>
				<dd>
					<a href="reportsActive.html">活跃设备</a>
				</dd>
				<dd>
					<a href="reportsRetention.html">留存设备</a>
				</dd>
				<dd>
					<a href="reportsLaunch.html">使用频率</a>
				</dd>
				<dd>
					<a href="reportsSecondsSpent.html">使用时长</a>
				</dd>
				<dd>
					<a href="reportsAreas.html">地域</a>
				</dd>
			</dl>
			<dl>
				<dt>
					终端及网络<b></b>
				</dt>
				<dd>
					<a href="reportsModels.html">设备</a>
				</dd>
				<dd>
					<a href="reportsOsVersion.html">操作系统</a>
				</dd>
				<dd>
					<a href="reportsResolution.html">分辨率</a>
				</dd>
				<dd>
					<a href="reportsCarrier.html">运营商</a>
				</dd>
				<dd>
					<a href="reportsNetWorks.html">联网方式</a>
				</dd>
			</dl>
			<dl>
				<dt>
					错误分析<b></b>
				</dt>
				<dd>
					<a href="exceptions.html">错误分析</a>
				</dd>
			</dl>
		</div>
	</div>
	<div id="page_footer_wrapper">
		<div id="page_footer">
			designed by <a href="http://git.heartleaves.com/" target="_blank">clover</a>&#0153;
			<br />
		</div>
	</div>
</body>
<script type="text/javascript" src="<c:url value='/js/Highcharts-2.2.5/highcharts.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/application.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/dhtmlgoodies_calendar.js'/>"></script>
</html>
