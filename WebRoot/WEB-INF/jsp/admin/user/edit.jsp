<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../../inc/header.jsp"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>修改用户</title>
<script type="text/javascript">
	$().ready(function() {
		$("form:first").validate();
	});
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/My97DatePicker/WdatePicker.js"></script>
</head>
<body>

	<div id="page_header_wrapper">
		<div id="page_header">
			<div class="user">
				admin <a href="../logout">退出登录</a>
			</div>
			<div class="logo">shake</div>
		</div>
	</div>
	<div id="page_content">
		<div id="right_column">
			<div class="message"></div>
			<div class="table_form">
				<h3>概况</h3>
				<form:form action="edit" method="post" commandName="user" enctype="multipart/form-data">
				<form:hidden path="id" />
				<form:hidden path="udid" />
				<table width="98%" cellspacing="0" cellpadding="0" border="0" class="table_form">
					<tbody>
						<tr>
							<th width="100"><span class="c_red">*</span> UDID：</th>
							<td><form:input path="udid" size="40" cssClass="required" disabled="true" /></td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 用户密码：</th>
							<td><form:input path="password" size="40" cssClass="required" /></td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 用户来源：</th>
							<td>
								<form:select path="source">
									<form:option value="-1" label="--请选择--" />
						            <form:option value="0" label="注册用户" />
						            <form:option value="1" label="腾讯用户" />
						            <form:option value="2" label="新浪用户" />
						        </form:select>
						    </td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 用户姓名：</th>
							<td><form:input path="name" size="40" cssClass="required" /></td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 当前等级：</th>
							<td><form:input path="level" size="40" cssClass="{required:true, digits:true, maxlength:3}" /></td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 经验值：</th>
							<td><form:input path="exp" size="40" cssClass="{required:true, digits:true, maxlength:9}" /></td>
						</tr>
						<tr>
							<th><span class="c_red">*</span> 注册时间：</th>
							<td><!-- 不格式化的话日期显示会多个0提交的时候就会出错 -->
								<input type="text" name="time" value="<fmt:formatDate value="${user.time}" pattern="yyyy-MM-dd HH:mm:ss" />"
                                	readonly="readonly"
                                	class="Wdate"
                                	onFocus="WdatePicker({skin:'default',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>(格式2012-12-21 23:59:59)<br/>
                            </td>
						</tr>
						<c:if test="${user.avatar != null }">
						<tr id="avatar">
							<th><span class="c_red">*</span> 用户头像：</th>
							<td>
							<div id="avatarThumb">
								<ul>
								<li>
									<img src="/shakewebservice/${user.avatar }"/>
									<div><a id="${user.id }" href="javascript:void(0)">删除</a></div>
								</li>		
								</ul>
							</div>
							</td>
						</tr>
						</c:if>	
						<tr>
							<th><span class="c_red">*</span> 个人签名：</th>
							<td><form:textarea path="signature" cssStyle="width:430px;height:80px;" cssClass="required" /></td>
						</tr>
						<tr>
							<th></th>
							<td>
								<input type="submit" value="修改用户" class="none" />
							</td>
						</tr>
					</tbody>
				</table>
				</form:form>
			</div>
			<div id="exceptions_list_block" class="ajax_pager_container">
			</div>
		</div>
		<div id="page_menu">
			<dl>
				<dt>
					用户管理<b></b>
				</dt>
				<dd>
					<a href="<c:url value='/admin/user/regist'/>">注册用户</a>
				</dd>
				<dd>
					<a href="<c:url value='/admin/user/tqq'/>">腾讯微博</a>
				</dd>
				<dd>
					<a href="<c:url value='/admin/user/tsina'/>">新浪微博</a>
				</dd>
			</dl>
			<dl>
				<dt>
					商品管理<b></b>
				</dt>
				<dd>
					<a href="<c:url value='/admin/product/list'/>">所有商品</a>
				</dd>
				<dd>
					<a href="reportsRetention.html">留存设备</a>
				</dd>
				<dd>
					<a href="reportsLaunch.html">使用频率</a>
				</dd>
				<dd>
					<a href="reportsSecondsSpent.html">使用时长</a>
				</dd>
				<dd>
					<a href="reportsAreas.html">地域</a>
				</dd>
			</dl>
			<dl>
				<dt>
					终端及网络<b></b>
				</dt>
				<dd>
					<a href="reportsModels.html">设备</a>
				</dd>
				<dd>
					<a href="reportsOsVersion.html">操作系统</a>
				</dd>
				<dd>
					<a href="reportsResolution.html">分辨率</a>
				</dd>
				<dd>
					<a href="reportsCarrier.html">运营商</a>
				</dd>
				<dd>
					<a href="reportsNetWorks.html">联网方式</a>
				</dd>
			</dl>
			<dl>
				<dt>
					错误分析<b></b>
				</dt>
				<dd>
					<a href="exceptions.html">错误分析</a>
				</dd>
			</dl>
		</div>
	</div>
	<div id="page_footer_wrapper">
		<div id="page_footer">
			designed by <a href="http://git.heartleaves.com/" target="_blank">clover</a>&#0153;
			<br />
		</div>
	</div>
</body>
<script type="text/javascript"
	src="<c:url value='/js/Highcharts-2.2.5/highcharts.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/js/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/application.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/js/dhtmlgoodies_calendar.js'/>"></script>
</html>