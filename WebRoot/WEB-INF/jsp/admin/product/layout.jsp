<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../../inc/header.jsp"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>shake backend</title>
</head>
<body>

	<div id="page_header_wrapper">
		<div id="page_header">
			<div class="user">
				admin <a href="../logout">退出登录</a>
			</div>
			<div class="logo">shake</div>
		</div>
	</div>
	<div id="page_content">
		<div id="right_column">
			<div class="message"></div>
			<h1>
			搜索
            <div id="search" class="search_style">
						<form:form action="search" method="get" onsubmit="return checkID()" command="productSearch" style="height: 16px">
							搜索商品：
							<select name="field" id="requirement">
								<option value="name">
									按名称
								</option>
								<option value="id">
									按ID
								</option>
							</select>
							<input type="text" name="query" class="query" id="condition" />
							<input type="submit" class="button" />
						</form:form>
					</div>    	
			</h1>
			<div class="block">
				<table class='retention'>
					<thead>
						<tr>
							<th width="5%">ID</th>
							<th width="15%">商品名</th>
							<th width="20%">上架时间</th>
							<th width="20%">下架时间</th>
							<th width="15%">经度</th>
							<th width="15%">纬度</th>
							<th width="10%">管理操作</th>
						</tr>
					</thead>
					<c:forEach items="${ page.items }" var="product" varStatus="status">
						<tr>
							<td>${product.id }</td>
							<td>${product.name }</td>
							<td><fmt:formatDate value="${product.availableBegin }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td><fmt:formatDate value="${product.availableEnd }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td>${product.longitude }</td>
							<td>${product.latitude }</td>
							<td><a href="edit?id=${product.id }">编辑</a> | <a href="delete/id/${product.id }"
								onclick="return confirm('确定要删除?')">删除</a>
							</td>
						</tr>
					</c:forEach>
				</table>
				<div class="table_foot">
					<p class="f_l"><a href="add">添加商品</a><!-- 总计: ${page.context.totalCount } --></p>
					<p class="f_r">
						<common:navigationTag url="/admin/product/${from }" />
					</p>
				</div>
			</div>
		</div>
		<div id="page_menu">
			<dl>
				<dt>
					用户管理<b></b>
				</dt>
				<dd>
					<a href="<c:url value='/admin/user/regist'/>">注册用户</a>
				</dd>
				<dd>
					<a href="<c:url value='/admin/user/tqq'/>">腾讯微博</a>
				</dd>
				<dd>
					<a href="<c:url value='/admin/user/tsina'/>">新浪微博</a>
				</dd>
			</dl>
			<dl>
				<dt>
					商品管理<b></b>
				</dt>
				<dd>
					<a href="<c:url value='/admin/product/list'/>">所有商品</a>
				</dd>
				<dd>
					<a href="reportsRetention.html">留存设备</a>
				</dd>
				<dd>
					<a href="reportsLaunch.html">使用频率</a>
				</dd>
				<dd>
					<a href="reportsSecondsSpent.html">使用时长</a>
				</dd>
				<dd>
					<a href="reportsAreas.html">地域</a>
				</dd>
			</dl>
			<dl>
				<dt>
					终端及网络<b></b>
				</dt>
				<dd>
					<a href="reportsModels.html">设备</a>
				</dd>
				<dd>
					<a href="reportsOsVersion.html">操作系统</a>
				</dd>
				<dd>
					<a href="reportsResolution.html">分辨率</a>
				</dd>
				<dd>
					<a href="reportsCarrier.html">运营商</a>
				</dd>
				<dd>
					<a href="reportsNetWorks.html">联网方式</a>
				</dd>
			</dl>
			<dl>
				<dt>
					错误分析<b></b>
				</dt>
				<dd>
					<a href="exceptions.html">错误分析</a>
				</dd>
			</dl>
		</div>
	</div>
	<div id="page_footer_wrapper">
		<div id="page_footer">
			designed by <a href="http://git.heartleaves.com/" target="_blank">clover</a>&#0153;
			<br />
		</div>
	</div>
</body>
<script type="text/javascript"
	src="<c:url value='/js/Highcharts-2.2.5/highcharts.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/js/jquery.blockUI.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/application.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/js/dhtmlgoodies_calendar.js'/>"></script>
</html>