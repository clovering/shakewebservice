package com.clover.scaffold.service.impl;

import java.util.List;

import lombok.Getter;

import com.clover.scaffold.Constants;
import com.clover.scaffold.dao.IBaseDao;
import com.clover.scaffold.pagination.Page;
import com.clover.scaffold.pagination.PageUtil;
import com.clover.scaffold.service.IBaseService;

@Getter
public abstract class BaseServiceImpl<M extends java.io.Serializable, PK extends java.io.Serializable> implements
		IBaseService<M, PK> {
	
	protected IBaseDao<M, PK> baseDao;

	public abstract void setBaseDao(IBaseDao<M, PK> baseDao);

	@Override
	public M save(M model) {
		return get(baseDao.save(model));
	}

	@Override
	public void saveOrUpdate(M model) {
		baseDao.saveOrUpdate(model);
	}

	@Override
	public void update(M model) {
		baseDao.update(model);	
	}

	@Override
	public void merge(M model) {
		baseDao.merge(model);		
	}

	@Override
	public void delete(PK id) {
		baseDao.delete(id);		
	}

	@Override
	public M get(PK id) {
		return baseDao.get(id);
	}

	@Override
	public int countAll() {
		return baseDao.countAll();
	}

	@Override
	public List<M> listAll() {
		return baseDao.listAll();
	}

	@Override
	public Page<M> listAll(int pn) {
		return this.listAll(pn, Constants.DEFAULT_PAGE_SIZE);
	}
	
	@Override
	public Page<M> listAll(int pn, int pageSize) {
		Integer count = countAll();
		List<M> items = baseDao.listAll(pn, pageSize);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

}
