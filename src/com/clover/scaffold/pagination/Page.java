package com.clover.scaffold.pagination;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 表示分页中的一页。
 */
@Setter
@Getter
public class Page<E> {

	/** 是否是首页? 有没有前一页. */
	private boolean hasPre;

	/** 是否是尾页? 有么有后一页. */
	private boolean hasNext;

	/** 当前页包含的记录列表. */
	private List<E> items;

	/** 当前页页码(起始为1). */
	private int index;
	
	/** 分页上下文环境, 用于计算Page. */
	private IPageContext<E> context;
	
	public boolean isHasPre() {
		return this.hasPre;
	}

	public void setHasPre(boolean hasPre) {
		this.hasPre = hasPre;
	}

	public boolean isHasNext() {
		return this.hasNext;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public List<E> getItems() {
		return this.items == null ? Collections.<E> emptyList() : this.items;
	}

}
