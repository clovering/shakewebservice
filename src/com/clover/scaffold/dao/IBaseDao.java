package com.clover.scaffold.dao;

import java.io.Serializable;
import java.util.List;

public interface IBaseDao<M extends Serializable, PK extends Serializable> {

	public PK save(M model);
	
	public void saveOrUpdate(M model);
	
	public void update(M model);
	
	public void merge(M model);
	
	public void delete(PK id);
	
	public M get(PK id);
	
	public int countAll();
	
	public List<M> listAll();
	
	public List<M> listAll(int pn, int pageSize);
	
}
