package com.clover.shakews.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "_Product")
public class Product implements Serializable {

	@Transient
	private static final long serialVersionUID = -7684241294639455347L;

	@Id
	@Column(length = 11, nullable = false)
	private int id;
	private String name;
	private String description;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date availableBegin;	//商品开始上架时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date availableEnd;	//商品结束时间
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createAt;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateAt;
	private String poster;	//商品海报
	@Transient
	private MultipartFile posterFile;
	private double longitude;	//经度
	private double latitude;	//纬度
	
}
