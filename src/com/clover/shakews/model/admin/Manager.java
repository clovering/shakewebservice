package com.clover.shakews.model.admin;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Manager {

	private String name;
	private String password;

}
