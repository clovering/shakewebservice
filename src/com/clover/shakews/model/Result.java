package com.clover.shakews.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Result<E> {
	
	private int status;
	private String message;
	private E content;
	
}
