package com.clover.shakews.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "_User")
@JsonIgnoreProperties(value = { "password", "time", "avatarFile" })
public class User implements Serializable {

	@Transient
	private static final long serialVersionUID = 5645409808201547101L;

	@Id
	@Column(length = 11, nullable = false)
	private int id;
	private String udid;
	private String password;
	private String name;
	private String avatar;
	private int level;
	private int exp;
	private String signature; // 签名
	private int source; // 来源
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date time; // 创建时间
	@Transient
	private MultipartFile avatarFile;

	public User(int source) {
		this.source = source;
	}

}
