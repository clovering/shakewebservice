package com.clover.shakews.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UserSearch {

	private int pn = 1;
	private String field;
	private String query;
	
}
