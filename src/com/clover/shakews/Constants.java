package com.clover.shakews;

public class Constants {

	public static final String COMMAND_MANAGER = "manager";
	public static final String COMMAND_USER = "user";
	public static final String COMMAND_PRODUCT = "product";
	
	public static final String AVATAR_FOLDER = "avatar";
	public static final String POSTER_FOLDER = "poster";
	
	public static final String WEB_ROOT = "http://localhost:8080/shakewebservice/";
	
}
