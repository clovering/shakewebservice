package com.clover.shakews.web.api.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.log4j.Log4j;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.clover.shakews.util.RegexUtil;

@Log4j
public class AuthorityInterceptor extends HandlerInterceptorAdapter {

	private static final String LOGIN_URL = "/api/login";
	private static final String LOGOUT_URL = "/api/logout";
	private static final String REGIST_URL = "/api/regist";

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		log.debug(String.format("requestUrl: %s", request.getServletPath()));
		if (request.getServletPath().startsWith(LOGIN_URL) || request.getServletPath().startsWith(LOGOUT_URL)
				|| request.getServletPath().startsWith(REGIST_URL)) {
			return true;
		}
		if (request.getSession().getAttribute(RegexUtil.getUdid(request.getServletPath())) != null) {
			log.debug(String.format("currentUser: %s",
					request.getSession().getAttribute(RegexUtil.getUdid(request.getServletPath()))));
			return true;
		}
		response.sendRedirect(request.getContextPath() + LOGIN_URL);
		return true;
	}

}
