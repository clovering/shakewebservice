package com.clover.shakews.web.api.controller;

import java.util.List;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.clover.scaffold.pagination.Page;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.Result;
import com.clover.shakews.service.api.IProductService;
import com.clover.shakews.util.WrapperUtil;

@Controller("apiProductController")
@Log4j
@RequestMapping("api/product")
public class ProductController {

	@Autowired
	@Qualifier("apiProductService")
	private IProductService productService;
	
	@RequestMapping(value = "/list/pn/{pn}/udid/{udid}")
	public @ResponseBody Result<List<Product>> list(@PathVariable("pn") int pn) {
		log.debug(String.format("pn: %d", pn));
		
		Result<List<Product>> result = null;
		Page<Product> page = productService.listAll(pn);
		List<Product> products = page == null || page.getItems().isEmpty() ? null : page.getItems();
		if (products != null) {
			result = new Result<List<Product>>(1, "success", WrapperUtil.wrapper(products));
		} else {
			result = new Result<List<Product>>(0, "fail", products);
		}
		return result;
	}
	
}
