package com.clover.shakews.web.api.controller;

import javax.servlet.http.HttpSession;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.clover.shakews.model.Result;
import com.clover.shakews.model.User;
import com.clover.shakews.service.api.IUserService;
import com.clover.shakews.util.WrapperUtil;

@Controller("apiLoginController")
@Log4j
public class LoginController {

	@Autowired
	@Qualifier("apiUserService")
	private IUserService userService;

	@RequestMapping(value = "/api/login/udid/{udid}/password/{password}")
	public @ResponseBody
	Result<User> login(@PathVariable("udid") String udid, @PathVariable("password") String password, HttpSession session) {
		log.debug(String.format("udid: %s, password: %s", udid, password));

		Result<User> result = null;
		User user = userService.valid(udid, password);
		if (user != null) {
			session.setAttribute(udid, udid);
			result = new Result<User>(1, "success", WrapperUtil.wrapper(user));
		} else {
			result = new Result<User>(0, "fail", user);
		}
		return result;
	}

	@RequestMapping(value = "/api/regist/udid/{udid}/name/{name}/password/{password}/confirmPassword/{confirmPassword}/source/{source}")
	public @ResponseBody
	Result<User> regist(@PathVariable("udid") String udid, @PathVariable("name") String name,
			@PathVariable("password") String password, @PathVariable("confirmPassword") String confirmPassword,
			@PathVariable("source") int source, HttpSession session) {
		Result<User> result = null;
		User user = userService.regist(udid, name, password, confirmPassword, source);
		if (user != null) {
			session.setAttribute(udid, udid);
			result = new Result<User>(1, "success", WrapperUtil.wrapper(user));
		} else {
			result = new Result<User>(0, "fail", user);
		}
		return result;
	}

}
