package com.clover.shakews.web.api.controller;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.clover.shakews.model.Result;
import com.clover.shakews.model.User;
import com.clover.shakews.service.api.IUserService;
import com.clover.shakews.util.WrapperUtil;

@Controller("apiUserController")
@Log4j
@RequestMapping("api/user")
public class UserController {

	@Autowired
	@Qualifier("apiUserService")
	private IUserService userService;
	
	@RequestMapping(value = "/show/showId/{showId}/udid/{udid}")
	public @ResponseBody Result<User> show(@PathVariable("showId") String showId) {
		log.debug(String.format("showId: %s", showId));
		
		Result<User> result = null;
		User user = userService.getByUdid(showId);
		if (user != null) {
			result = new Result<User>(1, "success", WrapperUtil.wrapper(user));
		} else {
			result = new Result<User>(0, "fail", user);
		}
		return result;
	}	
	
}
