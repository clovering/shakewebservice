package com.clover.shakews.web.admin.controller;

import java.util.Map;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.clover.shakews.Constants;
import com.clover.shakews.model.admin.Manager;
import com.clover.shakews.service.admin.IManagerService;

@Controller("adminLoginController")
@Log4j
@SessionAttributes("managerName")
public class LoginController {

	@Autowired
	private IManagerService managerService;

	/**
	 * 初始化表单 GET
	 */
	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public String toLogin(ModelMap model) {
		Manager manager = new Manager();
		model.addAttribute(Constants.COMMAND_MANAGER, manager);
		return "/admin/login";
	}

	/**
	 * 登录 POST
	 */
	@RequestMapping(value = "/admin/login", method = RequestMethod.POST)
	public String login(@ModelAttribute(Constants.COMMAND_MANAGER) Manager manager, ModelMap model) {
		log.debug(manager);
		if (managerService.valid(manager)) {
			model.put("managerName", manager.getName());
			return "redirect:/admin/user/regist";
		}
		return "redirect:/admin/login";
	}
	
	/**
	 * 退出GET
	 */
	@RequestMapping(value="/admin/logout", method = RequestMethod.GET)
	public String logout(Map<String, Object> model, SessionStatus status) {
		status.setComplete();
		return "redirect:/admin/login";
	}

}
