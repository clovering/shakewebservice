package com.clover.shakews.web.admin.controller;

import javax.servlet.http.HttpSession;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.clover.scaffold.pagination.Page;
import com.clover.shakews.Constants;
import com.clover.shakews.model.User;
import com.clover.shakews.model.UserSearch;
import com.clover.shakews.service.admin.IUserService;

@Controller("adminUserController")
@Log4j
@RequestMapping("admin/user")
public class UserController {

	@Autowired
	@Qualifier("adminUserService")
	private IUserService userService;
	
	// add
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String toAdd(ModelMap model) {
		User user = new User();
		model.addAttribute(Constants.COMMAND_USER, user);
		return "/admin/user/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute(Constants.COMMAND_USER) User user, HttpSession session) {
		String webRoot = session.getServletContext().getRealPath("/");
		userService.save(webRoot, user);
		return "redirect:/admin/user/regist";
	}
	
	// delete
	@RequestMapping(value = "/delete/id/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id) {
		userService.delete(id);
		return "redirect:/admin/user/regist";
	}
	
	// edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView toEdit(@RequestParam("id") int id, ModelMap model) {
		User user = userService.get(id);
		model.addAttribute(Constants.COMMAND_USER, user);
		return new ModelAndView("/admin/user/edit").addObject(model);
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	//  使用这种方式即单独把上传参数独立出来，在html中没有上传表单时会出现问题，即跳转显示页面，用户没有打算更改头像直接提交就会出现问题。
	//	public String edit(@RequestParam("avatarFile") MultipartFile avatarFile, @ModelAttribute(Constants.COMMAND_USER) User user, HttpSession session) {
	public String edit(@ModelAttribute(Constants.COMMAND_USER) User user, HttpSession session) {
		String webRoot = session.getServletContext().getRealPath("/");
		userService.update(webRoot, user);
		return "redirect:/admin/user/regist";
	}
	
	// search
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model, @ModelAttribute("userSearch") UserSearch userSearch) {
		log.debug(userSearch);
		Page<User> page = null;
		if (userSearch.getField().equals("name")) {
			page = userService.query(userSearch);
		} else {
			page = userService.query(userSearch);
		}
		model.put("page", page);
		model.put("from", "search?field=" + userSearch.getField() + "&query=" + userSearch.getQuery() + "&");
		return new ModelAndView("/admin/user/layout").addObject(model);
	}

	// index
	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	public ModelAndView regist(@RequestParam(value = "pn", defaultValue = "1") int pn, ModelMap model) {
		model.put("page", userService.listRegist(pn));
		model.put("from", "regist");
		return new ModelAndView("/admin/user/layout").addAllObjects(model);
	}

	@RequestMapping(value = "/tqq", method = RequestMethod.GET)
	public ModelAndView tqq(@RequestParam(value = "pn", defaultValue = "1") int pn, ModelMap model) {
		model.put("page", userService.listTQQ(pn));
		model.put("from", "tqq");
		return new ModelAndView("/admin/user/layout").addAllObjects(model);
	}
	
	@RequestMapping(value = "/tsina", method = RequestMethod.GET)
	public ModelAndView tsina(@RequestParam(value = "pn", defaultValue = "1") int pn, ModelMap model) {
		model.put("page", userService.listTSina(pn));
		model.put("from", "tsina");
		return new ModelAndView("/admin/user/layout").addAllObjects(model);
	}
	
}
