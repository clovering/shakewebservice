package com.clover.shakews.web.admin.controller;

import javax.servlet.http.HttpSession;

import lombok.extern.log4j.Log4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.clover.scaffold.pagination.Page;
import com.clover.shakews.Constants;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.ProductSearch;
import com.clover.shakews.service.admin.IProductService;

@Controller("adminProductController")
@Log4j
@RequestMapping("admin/product")
public class ProductController {

	@Autowired
	@Qualifier("adminProductService")
	private IProductService productService;

	// add
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String toAdd(ModelMap model) {
		Product product = new Product();
		model.addAttribute(Constants.COMMAND_PRODUCT, product);
		return "/admin/product/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@ModelAttribute(Constants.COMMAND_PRODUCT) Product product, HttpSession session) {
		String webRoot = session.getServletContext().getRealPath("/");
		productService.save(webRoot, product);
		return "redirect:/admin/product/list";
	}

	// delete
	@RequestMapping(value = "/delete/id/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") int id) {
		productService.delete(id);
		return "redirect:/admin/product/list";
	}

	// edit
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView toEdit(@RequestParam("id") int id, ModelMap model) {
		Product product = productService.get(id);
		model.addAttribute(Constants.COMMAND_PRODUCT, product);
		return new ModelAndView("/admin/product/edit").addObject(model);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edit(@ModelAttribute(Constants.COMMAND_PRODUCT) Product product, HttpSession session) {
		String webRoot = session.getServletContext().getRealPath("/");
		productService.update(webRoot, product);
		return "redirect:/admin/product/list";
	}

	// search
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model, @ModelAttribute("productSearch") ProductSearch productSearch) {
		log.debug(productSearch);
		Page<Product> page = null;
		if (productSearch.getField().equals("name")) {
			page = productService.query(productSearch);
		} else {
			page = productService.query(productSearch);
		}
		model.put("page", page);
		model.put("from", "search?field=" + productSearch.getField() + "&query=" + productSearch.getQuery() + "&");
		return new ModelAndView("/admin/product/layout").addObject(model);
	}

	// index
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(value = "pn", defaultValue = "1") int pn, ModelMap model) {
		model.put("page", productService.listAll(pn));
		model.put("from", "list");
		return new ModelAndView("/admin/product/layout").addAllObjects(model);
	}

}
