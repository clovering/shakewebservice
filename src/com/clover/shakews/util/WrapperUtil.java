package com.clover.shakews.util;

import java.util.List;

import com.clover.shakews.Constants;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.User;

public class WrapperUtil {

	public static List<Product> wrapper(List<Product> products) {
		for (Product product : products) {
			product.setPoster(product.getPoster() == null || product.getPoster().isEmpty() ? product.getPoster()
					: Constants.WEB_ROOT + product.getPoster());
		}
		return products;
	}

	public static User wrapper(User user) {
		user.setAvatar(user.getAvatar() == null || user.getAvatar().isEmpty() ? user.getAvatar() : Constants.WEB_ROOT
				+ user.getAvatar());
		return user;
	}

}

// public class WrapperUtil<M> {
//
// public List<M> wrapper(List<M> from) {
// for (M obj : from) {
// if (obj instanceof User) {
// ((User) obj).setAvatar(Constants.WEB_ROOT + ((User) obj).getAvatar());
// }
// if (obj instanceof Product) {
// ((Product) obj).setPoster(Constants.WEB_ROOT + ((Product) obj).getPoster());
// }
// }
// return from;
// }
//
// }
