package com.clover.shakews.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

	public static String getUdid(String url) {
        String udid = null;
        String regex = "udid/(.*)/";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(url);
        while (m.find()) {
        	udid = m.group(1);
        }
        if (udid != null) {
        	return udid;
        }
        regex = "udid/(.*)";
        p = Pattern.compile(regex);
        m = p.matcher(url);
        while (m.find()) {
        	udid = m.group(1);
        }
        return udid;
	}
	
}
