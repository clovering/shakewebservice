package com.clover.shakews.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import lombok.extern.log4j.Log4j;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.clover.shakews.Constants;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.User;

@Log4j
public class FileUtil {

	public static String uploadPoster(String webRoot, Product product, MultipartFile posterFile) {
		String posterName = null;
		if (product != null && posterFile != null) {
			try {
				// generate new poster name
				String filename = posterFile.getOriginalFilename();
				String suffix = filename.substring(filename.lastIndexOf("."), filename.length());
				String longitude = String.valueOf(product.getLongitude());
				String latitude = String.valueOf(product.getLatitude());
				String newFilename = (longitude.contains(".") ? longitude.substring(0, longitude.indexOf("."))
						: longitude)
						+ (latitude.contains(".") ? latitude.substring(0, longitude.indexOf(".")) : latitude)
						+ TimeUtil.unixtime() + suffix;
				// create poster folder if not exists
				String tempPath = File.separator + TimeUtil.year() + File.separator + TimeUtil.month() + File.separator
						+ TimeUtil.day() + File.separator;
				String folderPath = Constants.POSTER_FOLDER + tempPath;
				newFolder(webRoot + folderPath); // new folder
				// copy file from inputstream to destination
				FileOutputStream fos = new FileOutputStream(webRoot + folderPath + newFilename);
				InputStream fis = posterFile.getInputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = fis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				fis.close();

				posterName = tempPath + newFilename;
			} catch (Exception e) {
				log.debug(ExceptionUtils.getFullStackTrace(e));
			}
		}
		return posterName;
	}

	public static String uploadAvatar(String webRoot, User user, MultipartFile avatarFile) {
		String avatarName = null;
		if (user != null && avatarFile != null) {
			try {
				// generate new avatar name
				String filename = avatarFile.getOriginalFilename();
				String suffix = filename.substring(filename.lastIndexOf("."), filename.length());
				String newFilename = user.getUdid() + TimeUtil.unixtime() + suffix;
				// create avatar folder if not exists
				String tempPath = File.separator + TimeUtil.year() + File.separator + TimeUtil.month() + File.separator
						+ TimeUtil.day() + File.separator;
				String folderPath = Constants.AVATAR_FOLDER + tempPath;
				newFolder(webRoot + folderPath); // new folder
				// copy file from inputstream to destination
				FileOutputStream fos = new FileOutputStream(webRoot + folderPath + newFilename);
				InputStream fis = avatarFile.getInputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = fis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				fis.close();

				avatarName = tempPath + newFilename;
			} catch (Exception e) {
				log.debug(ExceptionUtils.getFullStackTrace(e));
			}
		}
		return avatarName;
	}

	private static void newFolder(String folderPath) {
		File folder = new File(folderPath);
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}

}
