package com.clover.shakews.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeUtil {
	
	public static int unixtimeTheDayBeforeYesterday() {
		return unixtime() - 2 * 24 * 60 * 60;
	}

	public static int unixtime() {
		return (int) (System.currentTimeMillis() / 1000L);
	}

	public static int year() {
		return calGenerator(Calendar.YEAR);
	}

	public static int month() {
		return calGenerator(Calendar.MONTH) + 1;
	}

	public static int day() {
		return calGenerator(Calendar.DAY_OF_MONTH);
	}

	private static int calGenerator(int type) {
		GregorianCalendar g = new GregorianCalendar();
		return (int) g.get(type);
	}

	public static String timestampToDate(long tempstamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	//hh:mm:ss为12进制HH:mm:ss为24进制
		return sdf.format(new Date(tempstamp * 1000));
	}
	
	public static int dateToTimestamp(String date) {
		Timestamp ts = Timestamp.valueOf(date);
		return (int) (ts.getTime() / 1000);
	}

}
