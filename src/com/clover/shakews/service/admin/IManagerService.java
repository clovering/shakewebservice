package com.clover.shakews.service.admin;

import org.springframework.transaction.annotation.Transactional;

import com.clover.shakews.model.admin.Manager;

@Transactional
public interface IManagerService {

	boolean valid(Manager manager);
	
}
