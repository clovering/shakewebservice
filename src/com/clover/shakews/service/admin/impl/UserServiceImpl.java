package com.clover.shakews.service.admin.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.clover.scaffold.Constants;
import com.clover.scaffold.dao.IBaseDao;
import com.clover.scaffold.pagination.Page;
import com.clover.scaffold.pagination.PageUtil;
import com.clover.scaffold.service.impl.BaseServiceImpl;
import com.clover.shakews.dao.IUserDao;
import com.clover.shakews.model.User;
import com.clover.shakews.model.UserQuery;
import com.clover.shakews.model.UserSearch;
import com.clover.shakews.service.admin.IUserService;
import com.clover.shakews.util.FileUtil;

@Service("adminUserService")
public class UserServiceImpl extends BaseServiceImpl<User, Integer> implements IUserService {

	private IUserDao userDao;

	@Autowired
	@Qualifier("userDao")
	@Override
	public void setBaseDao(IBaseDao<User, Integer> userDao) {
		this.baseDao = userDao;
		this.userDao = (IUserDao) userDao;
	}
	
	@Override
	public int countQuery(UserSearch userSearch) {
		int count = 0;
		if (userSearch.getField().equals("name")) {
			count = userDao.countByName(userSearch);
		} else {
			count = userDao.countByUdid(userSearch);
		}
		return count;
	}
	
	@Override
	public Page<User> query(UserSearch userSearch) {
		int count = countQuery(userSearch);
		List<User> items = null;
		if (userSearch.getField().equals("name")) {
			items = userDao.queryByName(userSearch.getPn(), Constants.DEFAULT_PAGE_SIZE, userSearch);
		} else {
			items = userDao.queryByUdid(userSearch.getPn(), Constants.DEFAULT_PAGE_SIZE, userSearch);
		}
		return PageUtil.getPage(count, userSearch.getPn(), items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public List<User> listRegist() {
		return userDao.listBySource(new UserQuery(0));
	}

	@Override
	public List<User> listTQQ() {
		return userDao.listBySource(new UserQuery(1));
	}

	@Override
	public List<User> listTSina() {
		return userDao.listBySource(new UserQuery(2));
	}

	@Override
	public int countRegist() {
		return userDao.countBySource(new UserQuery(0));
	}

	@Override
	public int countTQQ() {
		return userDao.countBySource(new UserQuery(1));
	}

	@Override
	public int countTSina() {
		return userDao.countBySource(new UserQuery(2));
	}

	@Override
	public Page<User> listRegist(int pn) {
		Integer count = countRegist();
		List<User> items = userDao.listBySource(pn, Constants.DEFAULT_PAGE_SIZE, new UserQuery(0));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<User> listTQQ(int pn) {
		Integer count = countTQQ();
		List<User> items = userDao.listBySource(pn, Constants.DEFAULT_PAGE_SIZE, new UserQuery(1));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<User> listTSina(int pn) {
		Integer count = countTSina();
		List<User> items = userDao.listBySource(pn, Constants.DEFAULT_PAGE_SIZE, new UserQuery(2));
		return PageUtil.getPage(count, pn, items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
//	public void update(String webRoot, User user, MultipartFile avatarFile) {
	public void update(String webRoot, User user) {
		if (user.getAvatarFile() != null) {
			// 拷贝文件
			String avatarName = FileUtil.uploadAvatar(webRoot, user, user.getAvatarFile());
			user.setAvatar(com.clover.shakews.Constants.AVATAR_FOLDER + avatarName);
			// 更新数据库, 调用BaseServiceImpl的update方法
			update(user);
		} else {
			userDao.updateWithOutAvatar(user);
		}
	}

	@Override
	public void save(String webRoot, User user) {
		if (user.getAvatarFile() != null) {
			// 拷贝文件
			String avatarName = FileUtil.uploadAvatar(webRoot, user, user.getAvatarFile());
			user.setAvatar(com.clover.shakews.Constants.AVATAR_FOLDER + avatarName);
			// 更新数据库, 调用BaseServiceImpl的update方法
			save(user);
		}
	}
	
}
