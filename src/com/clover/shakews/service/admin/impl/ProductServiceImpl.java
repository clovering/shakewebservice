package com.clover.shakews.service.admin.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.clover.scaffold.Constants;
import com.clover.scaffold.dao.IBaseDao;
import com.clover.scaffold.pagination.Page;
import com.clover.scaffold.pagination.PageUtil;
import com.clover.scaffold.service.impl.BaseServiceImpl;
import com.clover.shakews.dao.IProductDao;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.ProductSearch;
import com.clover.shakews.service.admin.IProductService;
import com.clover.shakews.util.FileUtil;

@Service("adminProductService")
public class ProductServiceImpl extends BaseServiceImpl<Product, Integer> implements IProductService {

	private IProductDao productDao;

	@Autowired
	@Qualifier("productDao")
	@Override
	public void setBaseDao(IBaseDao<Product, Integer> productDao) {
		this.baseDao = productDao;
		this.productDao = (IProductDao) productDao;
	}
	
	@Override
	public int countQuery(ProductSearch productSearch) {
		int count = 0;
		if (productSearch.getField().equals("name")) {
			count = productDao.countByName(productSearch);
		} else {
			count = productDao.countById(productSearch);
		}
		return count;
	}

	@Override
	public Page<Product> query(ProductSearch productSearch) {
		int count = countQuery(productSearch);
		List<Product> items = null;
		if (productSearch.getField().equals("name")) {
			items = productDao.queryByName(productSearch.getPn(), Constants.DEFAULT_PAGE_SIZE, productSearch);
		} else {
			items = productDao.queryById(productSearch.getPn(), Constants.DEFAULT_PAGE_SIZE, productSearch);
		}
		return PageUtil.getPage(count, productSearch.getPn(), items, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public void update(String webRoot, Product product) {
		if (product.getPosterFile() != null) {
			// 拷贝文件
			String posterName = FileUtil.uploadPoster(webRoot, product, product.getPosterFile());
			product.setPoster(com.clover.shakews.Constants.POSTER_FOLDER + posterName);
			// 更新数据库, 调用BaseServiceImpl的update方法
			update(product);
		} else {
			productDao.updateWithOutPoster(product);
		}
	}

	@Override
	public void save(String webRoot, Product product) {
		if (product.getPosterFile() != null) {
			// 拷贝文件
			String posterName = FileUtil.uploadPoster(webRoot, product, product.getPosterFile());
			product.setPoster(com.clover.shakews.Constants.POSTER_FOLDER + posterName);
			// 更新数据库, 调用BaseServiceImpl的update方法
			save(product);
		}
	}
	
}
