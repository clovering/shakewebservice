package com.clover.shakews.service.admin;

import java.util.List;

import com.clover.scaffold.pagination.Page;
import com.clover.scaffold.service.IBaseService;
import com.clover.shakews.model.User;
import com.clover.shakews.model.UserSearch;

public interface IUserService extends IBaseService<User, Integer> {
	
	public int countQuery(UserSearch userSearch);
	
	public Page<User> query(UserSearch userSearch);

	public List<User> listRegist();

	public List<User> listTQQ();
	
	public List<User> listTSina();
	
	public int countRegist();
	
	public int countTQQ();
	
	public int countTSina();
	
	public Page<User> listRegist(int pn);

	public Page<User> listTQQ(int pn);
	
	public Page<User> listTSina(int pn);

//	public void update(String webRoot, User user, MultipartFile avatarFile);
	public void update(String webRoot, User user);
	
	public void save(String webRoot, User user);
	
}