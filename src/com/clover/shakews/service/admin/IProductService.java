package com.clover.shakews.service.admin;

import com.clover.scaffold.pagination.Page;
import com.clover.scaffold.service.IBaseService;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.ProductSearch;

public interface IProductService extends IBaseService<Product, Integer> {
	
	public int countQuery(ProductSearch productSearch);

	public Page<Product> query(ProductSearch productSearch);

	public void update(String webRoot, Product product);

	public void save(String webRoot, Product product);

}
