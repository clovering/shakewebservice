package com.clover.shakews.service.api;

import com.clover.scaffold.service.IBaseService;
import com.clover.shakews.model.User;

public interface IUserService extends IBaseService<User, Integer> {
	
	public User valid(String udid, String password);

	public User getByUdid(String showId);

	public User regist(String udid, String name, String password, String confirmPassword, int source);
	
}