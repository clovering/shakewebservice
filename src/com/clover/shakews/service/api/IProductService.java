package com.clover.shakews.service.api;

import com.clover.scaffold.service.IBaseService;
import com.clover.shakews.model.Product;

public interface IProductService extends IBaseService<Product, Integer> {

}
