package com.clover.shakews.service.api.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.clover.scaffold.dao.IBaseDao;
import com.clover.scaffold.service.impl.BaseServiceImpl;
import com.clover.shakews.dao.IProductDao;
import com.clover.shakews.model.Product;
import com.clover.shakews.service.api.IProductService;

@Service("apiProductService")
public class ProductServiceImpl extends BaseServiceImpl<Product, Integer> implements IProductService {
	
	private IProductDao productDao;

	@Autowired
	@Qualifier("productDao")
	@Override
	public void setBaseDao(IBaseDao<Product, Integer> productDao) {
		this.baseDao = productDao;
		this.productDao = (IProductDao) productDao;
	}
	
}
