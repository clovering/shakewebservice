package com.clover.shakews.service.api.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.clover.scaffold.dao.IBaseDao;
import com.clover.scaffold.service.impl.BaseServiceImpl;
import com.clover.shakews.dao.IUserDao;
import com.clover.shakews.model.User;
import com.clover.shakews.service.api.IUserService;

@Service("apiUserService")
public class UserServiceImpl extends BaseServiceImpl<User, Integer> implements IUserService {

	private IUserDao userDao;

	@Autowired
	@Qualifier("userDao")
	@Override
	public void setBaseDao(IBaseDao<User, Integer> userDao) {
		this.baseDao = userDao;
		this.userDao = (IUserDao) userDao;
	}

	@Override
	public User valid(String udid, String password) {
		return userDao.queryByUdidAndPassword(udid, password);
	}

	@Override
	public User getByUdid(String showId) {
		return userDao.queryByUdid(showId);
	}

	@Override
	public User regist(String udid, String name, String password, String confirmPassword, int source) {
		if (udid == null || udid.trim().isEmpty() || name == null || name.trim().isEmpty() || password == null
				|| password.trim().isEmpty() || confirmPassword == null || confirmPassword.trim().isEmpty()) {
			return null;
		}
		if (!password.equals(confirmPassword)) {
			return null;
		}
		User user = new User();
		user.setUdid(udid);
		user.setName(name);
		user.setPassword(password);
		user.setSource(source);
		user.setTime(new Date());
		user.setAvatar("");
		user.setSignature("");
		user.setLevel(1);
		user.setExp(0);
		
		return save(user);
	}

}
