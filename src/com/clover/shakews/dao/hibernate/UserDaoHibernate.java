package com.clover.shakews.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.clover.scaffold.dao.hibernate.BaseDaoHibernate;
import com.clover.shakews.dao.IUserDao;
import com.clover.shakews.model.User;
import com.clover.shakews.model.UserQuery;
import com.clover.shakews.model.UserSearch;

@Repository("userDao")
public class UserDaoHibernate extends BaseDaoHibernate<User, Integer> implements IUserDao {

	private static final String HQL_LIST = "from User ";
	private static final String HQL_COUNT = "select count(*) ";
	private static final String HQL_ORDER_BY = "order by id asc";
	private static final String HQL_LIST_BY_SOURCE = HQL_LIST + "where source = ? " + HQL_ORDER_BY;
	private static final String HQL_COUNT_BY_PRICETREND = HQL_COUNT + HQL_LIST_BY_SOURCE;
	private static final String HQL_LIST_BY_NAME = HQL_LIST + "where name like ? " + HQL_ORDER_BY;
	private static final String HQL_LIST_BY_UDID = HQL_LIST + "where udid = ? " + HQL_ORDER_BY;
	private static final String HQL_COUNT_BY_NAME = HQL_COUNT + HQL_LIST_BY_NAME;
	private static final String HQL_COUNT_BY_UDID = HQL_COUNT + HQL_LIST_BY_UDID;
	
	private static final String HQL_LIST_BY_UDID_AND_PASSWORD = HQL_LIST + "where udid = ? and password = ?";
	
	@Override
	public User queryByUdid(String showId) {
		List<User> users = list(HQL_LIST_BY_UDID, getQueryParam(showId));
		if (users == null || users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}
	
	@Override
	public User queryByUdidAndPassword(String udid, String password) {
		List<User> users = list(HQL_LIST_BY_UDID_AND_PASSWORD, getQueryParam(udid, password));
		if (users == null || users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	@Override
	public void updateWithOutAvatar(User user) {
		Query query = getSession()
				.createQuery(
						"update User u set u.password = ?, u.source = ?, u.name = ?, u.level = ?, u.exp = ?, u.time = ?, u.signature = ? where u.id = ?");
		setParameters(
				query,
				getQueryParam(user.getPassword(), user.getSource(), user.getName(), user.getLevel(), user.getExp(),
						user.getTime(), user.getSignature(), user.getId()));
		query.executeUpdate();
	}

	@Override
	public int countByName(UserSearch userSearch) {
		return this.<Number> aggregate(HQL_COUNT_BY_NAME, getLikeParam(userSearch.getQuery())).intValue();
	}

	@Override
	public int countByUdid(UserSearch userSearch) {
		return this.<Number> aggregate(HQL_COUNT_BY_UDID, getQueryParam(userSearch.getQuery())).intValue();
	}

	@Override
	public List<User> queryByName(int pn, int pageSize, UserSearch userSearch) {
		return list(HQL_LIST_BY_NAME, pn, pageSize, getLikeParam(userSearch.getQuery()));
	}

	@Override
	public List<User> queryByUdid(int pn, int pageSize, UserSearch userSearch) {
		return list(HQL_LIST_BY_UDID, pn, pageSize, getQueryParam(userSearch.getQuery()));
	}

	@Override
	public List<User> listBySource(UserQuery userQuery) {
		return list(HQL_LIST_BY_SOURCE, getQueryParam(userQuery.getSource()));
	}

	@Override
	public int countBySource(UserQuery userQuery) {
		return this.<Number> aggregate(HQL_COUNT_BY_PRICETREND, getQueryParam(userQuery.getSource())).intValue();
	}

	@Override
	public List<User> listBySource(int pn, int pageSize, UserQuery userQuery) {
		return list(HQL_LIST_BY_SOURCE, pn, pageSize, getQueryParam(userQuery.getSource()));
	}

//	// 当做Object传入, 在这之前一定要做好类型转换!
//	private Object[] getLikeParam(Object... queryList) {
//		int size = queryList.length;
//		Object[] params = new Object[size];
//		for (int index = 0; index < size; index++) {
//			params[index] = "%" + queryList[index] + "%";
//		}
//		return params;
//	}
//
//	private Object[] getQueryParam(Object... queryList) {
//		return queryList;
//	}
	// private Object[] getLikeParam(Object query) {
	// return new Object[] { "%" + query + "%" };
	// }
	//
	// private Object[] getQueryParam(Object query) {
	// return new Object[] { query };
	// }

}
