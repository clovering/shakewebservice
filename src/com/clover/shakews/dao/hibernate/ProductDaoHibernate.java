package com.clover.shakews.dao.hibernate;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.clover.scaffold.dao.hibernate.BaseDaoHibernate;
import com.clover.shakews.dao.IProductDao;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.ProductSearch;

@Repository("productDao")
public class ProductDaoHibernate extends BaseDaoHibernate<Product, Integer> implements IProductDao {

	private static final String HQL_LIST = "from Product ";
	private static final String HQL_COUNT = "select count(*) ";
	private static final String HQL_ORDER_BY = "order by id asc";
	private static final String HQL_LIST_BY_NAME = HQL_LIST + "where name like ? " + HQL_ORDER_BY;
	private static final String HQL_LIST_BY_ID = HQL_LIST + "where id = ? " + HQL_ORDER_BY;
	private static final String HQL_COUNT_BY_NAME = HQL_COUNT + HQL_LIST_BY_NAME;
	private static final String HQL_COUNT_BY_ID = HQL_COUNT + HQL_LIST_BY_ID;

	@Override
	public int countByName(ProductSearch productSearch) {
		return this.<Number> aggregate(HQL_COUNT_BY_NAME, getLikeParam(productSearch.getQuery())).intValue();
	}

	@Override
	public int countById(ProductSearch productSearch) {
		String query = productSearch.getQuery();
		return this.<Number> aggregate(HQL_COUNT_BY_ID, getQueryParam(query.isEmpty() ? 0 : Integer.parseInt(query)))
				.intValue();
	}

	@Override
	public List<Product> queryByName(int pn, int pageSize, ProductSearch productSearch) {
		return list(HQL_LIST_BY_NAME, pn, pageSize, getLikeParam(productSearch.getQuery()));
	}

	@Override
	public List<Product> queryById(int pn, int pageSize, ProductSearch productSearch) {
		String query = productSearch.getQuery();
		return Arrays.asList(get(query.isEmpty() ? 0 : Integer.parseInt(query)));
	}

	@Override
	public void updateWithOutPoster(Product product) {
		Query query = getSession()
				.createQuery(
						"update Product p set p.name = ?, p.description = ?, p.availableBegin = ?, p.availableEnd = ?, p.createAt = ?, p.updateAt = ?, p.longitude = ?, p.latitude = ? where p.id = ?");
		setParameters(
				query,
				getQueryParam(product.getName(), product.getDescription(), product.getAvailableBegin(),
						product.getAvailableEnd(), product.getCreateAt(), product.getUpdateAt(),
						product.getLongitude(), product.getLatitude(), product.getId()));
		query.executeUpdate();
	}

}
