package com.clover.shakews.dao;

import java.util.List;

import com.clover.scaffold.dao.IBaseDao;
import com.clover.shakews.model.User;
import com.clover.shakews.model.UserQuery;
import com.clover.shakews.model.UserSearch;

public interface IUserDao extends IBaseDao<User, Integer> {
	
	int countByName(UserSearch userSearch);

	int countByUdid(UserSearch userSearch);

	List<User> queryByName(int pn, int pageSize, UserSearch userSearch);

	List<User> queryByUdid(int pn, int pageSize, UserSearch userSearch);

	List<User> listBySource(UserQuery userQuery);

	int countBySource(UserQuery userQuery);

	List<User> listBySource(int pn, int pageSize, UserQuery userQuery);
	
	void updateWithOutAvatar(User user);

	User queryByUdidAndPassword(String udid, String password);

	User queryByUdid(String showId);

}
