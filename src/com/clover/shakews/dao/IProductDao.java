package com.clover.shakews.dao;

import java.util.List;

import com.clover.scaffold.dao.IBaseDao;
import com.clover.shakews.model.Product;
import com.clover.shakews.model.ProductSearch;

public interface IProductDao extends IBaseDao<Product, Integer> {

	int countByName(ProductSearch productSearch);

	int countById(ProductSearch productSearch);

	List<Product> queryByName(int pn, int pageSize, ProductSearch productSearch);

	List<Product> queryById(int pn, int pageSize, ProductSearch productSearch);

	void updateWithOutPoster(Product product);

}
